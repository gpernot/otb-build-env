FROM ubuntu:19.04

ENV DEBIAN_FRONTEND noninteractive

RUN echo "Europe/Paris" > /etc/timezone

# ----------------------------------------------------------------------------
# First stage : install tools (they rarely evolve)
# ----------------------------------------------------------------------------
RUN apt-get update -y \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        make \
        cmake \
        g++ \
        gcc \
        git \
        git-lfs \
        libtool \
        swig \
        xvfb \
        ninja-build \
        ccache \
        lld \
        clang \
        clang-tidy \
        clang-format \
        llvm \
        llvm-runtime \
        llvm-dev \
        texlive-latex-base \
        texlive-latex-recommended \
        texlive-latex-extra \
        texlive-fonts-recommended \
        python3-numpy \
        python3-sphinx \
        python3-sphinx-rtd-theme \
        doxygen \
        graphviz \
        wget \
        gnuplot \
        dvipng \
 && rm -rf /var/lib/apt/lists/*

# Setup Git
RUN git config --global user.name "otbbot" \
 && git config --global user.email "otbbot@orfeo-toolbox.org" \
 && git lfs install --skip-repo
